Feature: /debitCard/checkFlagsTdd POST
    Obtener Informacion de Bandera de tarjeta de debito

    Scenario: Negocio un consumer key y secret key de la app de prueba
        Given I have basic authentication credentials `apigeeUsername` and `apigeePassword`
        And I have valid client TLS configuration
        When I GET `apigeeHost`/v1/organizations/`apigeeOrg`/developers/`apigeeDeveloper`/apps/`apigeeApp`
        Then response code should be 200
        And response body should be valid json
        And I store the value of body path $.credentials[0].consumerKey as globalConsumerKey in global scope
        And I store the value of body path $.credentials[0].consumerSecret as globalConsumerSecret in global scope

    Scenario: Negocia un access token con el Authorization server

        Given I set form parameters to
        | parameter  | value              |
        | grant_type | client_credentials |

        And I have basic authentication credentials `globalConsumerKey` and `globalConsumerSecret`
        And I have valid client TLS configuration
        When I POST to `apigeeDomain`/`apigeeOauthEndpoint`

        Then response code should be 200
        And response body should be valid json
        And I store the value of body path $.access_token as access token


    Scenario: Obtencion de llaves asimétricas
        Given I set bearer token
        And I have valid client TLS configuration
        And I set x-ismock header to true
        When  I GET `apigeeDomain`/operaciones-baz/seguridad/v1/aplicaciones/llaves
        Then response code should be 200
        And response body should be valid json
        And response body path $.mensaje should be ^([A-Za-zá-úÁ-Ú0-9\s,.-])+$
        And response body path $.folio should be ^[a-z0-9-]{1,}$
        And response body path $.resultado.idAcceso should be ^[a-zA-Z0-9]{1,60}$
        And I store the value of body path $.resultado.idAcceso as idAccess in global scope
        And I store the deciphering value encrypted with AES of body path $.resultado.accesoPublico as publicKey in global scope
        And I store the deciphering value encrypted with AES of body path $.resultado.accesoPrivado as privateKey in global scope
        And I store the deciphering value encrypted with AES of body path $.resultado.accesoSimetrico as accessSymmetric in global scope
        And I store the deciphering value encrypted with AES of body path $.resultado.codigoAutentificacionHash as codeHash in global scope

    Scenario Outline: Feature: /debitCard/checkFlagsTdd POST 200 ok
        Given I set bearer token
        And I set Content-Type header to application/json
        And I have valid client TLS configuration
        And I set x-ismock header to true
        #And I set x-id-acceso header to `idAccess`
        #And I set x-ip-origen header to <ipOrigen>
        #And I need to encrypt with RSA as algorithm the parameters {*}
        And I set body to <body>
        When I POST to `apigeeDomain`/dinn-restfull/debitCard/checkFlagsTdd
        Then response code should be 200
        And response body should be valid json
        And response body path $.mensaje should be ^[a-zA-Z]$
        And response body path $.statustdd should be ^[0-9]+$
        And response body path $.resultado.tid should be ^[a-zA-Z]+$

    Examples:
        |body                  |
        |{"clientNumber":1234"}|


    Scenario Outline:  /debitCard/checkFlagsTdd 400 bad request.

        Given I set bearer token
        And I set Content-Type header to application/json
        And I have valid client TLS configuration
        And I set x-ismock header to true
        And I set x-id-acceso header to `idAccess`
        And I set x-ip-origen header to <ipOrigen>
        And I set body to <body>
        When I POST to `apigeeDomain`/operaciones-baz/plataforma-aclaraciones/administracion-aclaraciones/`deploymentSuffix`/aclaraciones
        Then response code should be 400
        And response body should be valid json

        And response body path $.codigo should be ^400.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{4}$
        And response body path $.mensaje should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$
        And response body path $.folio should be [a-zA-Z0-9\W]{1,}
        And response body path $.info should be ^https:\/\/baz-developer\.bancoazteca\.com\.mx\/\w{4,6}#400\.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{0,}|[A-Z]{0,}\d{0,}$
        And response body path $.detalles[*] should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$

    Examples:
        |body                 |
        |{"clientNumber":400"}|

    Scenario Outline:  /debitCard/checkFlagsTdd 401 bad request.

        Given I set bearer token
        And I set Content-Type header to application/json
        And I have valid client TLS configuration
        And I set x-ismock header to true
        And I set x-id-acceso header to `idAccess`
        And I set x-ip-origen header to <ipOrigen>
        And I set body to <body>
        When I POST to `apigeeDomain`/operaciones-baz/plataforma-aclaraciones/administracion-aclaraciones/`deploymentSuffix`/aclaraciones
        Then response code should be 401
        And response body should be valid json

        And response body path $.codigo should be ^401.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{4}$
        And response body path $.mensaje should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$
        And response body path $.folio should be [a-zA-Z0-9\W]{1,}
        And response body path $.info should be ^https:\/\/baz-developer\.bancoazteca\.com\.mx\/\w{4,6}#401\.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{0,}|[A-Z]{0,}\d{0,}$
        And response body path $.detalles[*] should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$

    Examples:
        |body                 |
        |{"clientNumber":401"}|

    Scenario Outline:  /debitCard/checkFlagsTdd 404 not found.

        Given I set bearer token
        And I set Content-Type header to application/json
        And I have valid client TLS configuration
        And I set x-ismock header to true
        #And I set x-id-acceso header to `idAccess`
        #And I set x-ip-origen header to <ipOrigen>
        And I set body to <body>
        When I POST to `apigeeDomain`/operaciones-baz/plataforma-aclaraciones/administracion-aclaraciones/`deploymentSuffix`/aclaraciones
        Then response code should be 404
        And response body should be valid json

        And response body path $.codigo should be ^404.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{4}$
        And response body path $.mensaje should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$
        And response body path $.folio should be [a-zA-Z0-9\W]{1,}
        And response body path $.info should be ^https:\/\/baz-developer\.bancoazteca\.com\.mx\/\w{4,6}#404\.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{0,}|[A-Z]{0,}\d{0,}$
        And response body path $.detalles[*] should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$

    Examples:
        |body                 |
        |{"clientNumber":404"}|

    Scenario Outline: /debitCard/checkFlagsTdd 500 internal server error.

        Given I set bearer token
        And I set Content-Type header to application/json
        And I have valid client TLS configuration
        And I set x-ismock header to true
        #And I set x-id-acceso header to `idAccess`6
        #And I set x-ip-origen header to <ipOrigen>
        And I set body to <body>
        When I POST to `apigeeDomain`/operaciones-baz/plataforma-aclaraciones/administracion-aclaraciones/`deploymentSuffix`/aclaraciones

        Then response code should be 500
        And response body should be valid json

        And response body path $.codigo should be ^500.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{4}$
        And response body path $.mensaje should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$
        And response body path $.folio should be [a-zA-Z0-9\W]{1,}
        And response body path $.info should be ^https:\/\/baz-developer\.bancoazteca\.com\.mx\/\w{4,6}#500\.Operaciones-Baz-Plataforma-Aclaraciones-Gestion-Aclaraciones.\d{0,}|[A-Z]{0,}\d{0,}$
        And response body path $.detalles[*] should be ^[A-Za-záéíóúÁÉÍÓÚ0-9.,-\s:]{1,255}$

    Examples:
        |body                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
        |{"clientNumber":500"}|